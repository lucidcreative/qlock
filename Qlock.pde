import oscP5.*;
import netP5.*;
import controlP5.*;



ControlP5 cp5;
 
OscP5 oscP5;
NetAddress myRemoteLocation;
PFont f; 

//these are dummy values for offline testing
String currentCue="#Current Cue text is reallllllly long and sucks a lot", nextCue="Next", timeElapsed="01:02:03:04", timeRemaining="00:00:09:29";
final int LISTENING_PORT = 5954; //it seems like it doesn't care what this is?

final color RED = color(255, 200, 200);
final color GREEN = color(200, 255, 200);
final color FLASH_ON = color( 200 );
final color FLASH_OFF = color(0);

boolean isFullScreen = false;

void setup() {
  
  //fullScreen();
  //isFullScreen = true;
  //size(displayWidth,displayHeight);
  surface.setResizable(true);
 
  // start oscP5, telling it to listen for incoming messages at port 5954*/
  oscP5 = new OscP5(this, LISTENING_PORT);

  PFont font = createFont("arial",20);

  // setup display
  //cp5 = new ControlP5(this);
   
  //cp5.addBang("toggleFullScreen")
  //  .setPosition(20,height-60)
  //  .setSize(80,40)
  //  .setLabel("Toggle Full Screen")
  //  .getCaptionLabel().align(ControlP5.CENTER, ControlP5.CENTER)
  //  ;

  f = createFont("Arial",36,true);
   
  if( !checkLicense() ){
    exit();
  }
}
 
void draw()
{
  background(0);
  stroke(180, 180, 250);
  line(width*.5, 0, width*.5, height);
  
  //display current if the first character isn't a #
  if( currentCue.substring(0,1).equals( "#" ) == false){
    fill(200); 
    textFont(f,50);
    textAlign( CENTER, BOTTOM); 
    text( "Current Cue:", width*0.25, height*0.25);
    //print the cue text
    fill(255);
    textFont(f,width/max((currentCue.length()+1), 12));
    textAlign( CENTER, BOTTOM);
    text( currentCue, width*0.25, height*.5);
    //print the countup time
    textFont(f,width/12);
    textAlign( CENTER, TOP );
    text( timeElapsed, width*0.25, height*.5);
  }
  
  //display next cue if first character isn't a #
  if( nextCue.substring(0,1).equals( "#" ) == false){
    color nextColor, flash;
    int frames = framesRemaining(timeRemaining);
    
    if ( frames < 300 ){
      nextColor = RED;
    } else {
      nextColor = GREEN;
    }
    
    if ( frames < 7 ){
      flash = FLASH_ON;
    } else {
      flash = FLASH_OFF;
    }
    
    fill(200); 
    //header
    textFont(f,50);
    textAlign( CENTER, BOTTOM );
    text( "Next Cue:", width*0.75, height * 0.25);
    //print the next cue text
    fill(nextColor);
    textFont(f,width/max((nextCue.length()+1), 12));
    textAlign( CENTER, BOTTOM);
    text( nextCue, width*0.75, height*.5);
    //print the countdown time
    textFont(f,width/12);
    textAlign( CENTER, TOP );
    text( timeRemaining, width*0.75, height*.5);
    stroke(255);
  }
  //lucid watermark
  textFont(f,14);
  textAlign(RIGHT,BOTTOM);
  fill( 30,102,128); //lucid middle cyan
  text( "Start a timeline note with '#' to hide it. Port=5954\nd3 Qlock v1 - Lucid Creative 2016" , width*.97, height*.97);
 
}
 
//void mousePressed() {  
//  // create an osc message
//  OscMessage myMessage = new OscMessage("/test");
// 
//  myMessage.add(123); // add an int to the osc message
//  myMessage.add(12.34); // add a float to the osc message 
//  myMessage.add("some text!"); // add a string to the osc message
// 
//  // send the message
//  oscP5.send(myMessage, myRemoteLocation); 
//}
 
boolean checkLicense(){
  /*
  allow only until 12/13/17
  */
  
  if( month() == 12 && day() <= 13 && year() == 2017 ){
    return true;
  }
  else {
    return false;
  }
}
  
 void oscEvent(OscMessage theOscMessage) {
  /* print the address pattern and the typetag of the received OscMessage */
  //println( theOscMessage.addrPattern());
  if(theOscMessage.checkAddrPattern("/d3/showcontrol/sectionhint")){
    //print the message to log
    println( "OSC: " + theOscMessage.get(0));
    
    String[] msg = theOscMessage.get(0).toString().split("    ");
    currentCue = msg[1].substring(0, msg[1].length()-13); //13chr in the timecode
    nextCue = msg[2].substring(0, msg[2].length()-13);
    timeElapsed = msg[1].substring(currentCue.length()+2);
    timeRemaining = msg[2].substring(nextCue.length()+2);
    
    //println( currentCue );
    //println( nextCue );
    //println( timeElapsed );
    //println( timeRemaining );
    //.replace("+", "\n\t\t+").replace("-", "\n\t\t+");
  }
//  
//  print("### received an osc message.");
//  print(" addrpattern: "+theOscMessage.addrPattern());
//  println(" typetag: "+theOscMessage.typetag());
} 

int framesRemaining( String timecode ){
  //check to make sure we have a valid timecode!
  if( timecode.length() != 11 ){ return 0; } else {
    
    int h,m,s,f;
    h = int(timecode.substring(0,2));
    m = int(timecode.substring(3,5));
    s = int(timecode.substring(6,8));
    f = int(timecode.substring(9,11));
    
    int total = f + 30 * ( s + 60 * ( m + 60 * h));
    //println(h+":"+m+":"+s+":"+f);
    return total;
  }
}

void toggleFullScreen(){
  //toggles between full screen and maximized using cp5 bang
  if( isFullScreen ){
    surface.setSize(displayWidth, displayHeight);
    surface.setResizable(true);
    isFullScreen = false;
  } else {
    fullScreen();
    isFullScreen = true;
  }
}